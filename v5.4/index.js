const count = 1000;


let stockTime = document.querySelectorAll('.stock-block')
let bpopup = document.querySelector('.b-popup-content');


function openPopupForBuyItem() {
    bpopup.style.display = 'block';
}

function closePopup(e) {
    if(e.target != bpopup && 
       e.target != bpopup.querySelector('.container-popup') &&
       e.target != bpopup.querySelector('.close-popup')){

        return false
    }
    bpopup.style.display = 'none';
    // if(!bpopup.compareDocumentPosition(e.target)) bpopup.style.display = 'none';
    // console.log(!bpopup.compareDocumentPosition(e.target))
}

bpopup.addEventListener('click', closePopup, false)

let max =  2592000000;
let min = 172800000;

// рандомно созданные дни
function randomNumber(){
    let rand = new Date().getTime() +( min - 0.5 + Math.random() * (max - min + 1));
    rand = Math.round(rand);
    return rand;
}

// кол-во оставшихся дней, часов, минут, секунд

function getCountdown(date) {
    target_date = date;
    var days, hours, minutes, seconds;
    return function () {
        var current_date = new Date().getTime();
        var seconds_left = (target_date - current_date) / 1000;

        days = pad(parseInt(seconds_left / 86400));
        seconds_left = seconds_left % 86400;

        hours = pad(parseInt(seconds_left / 3600));
        seconds_left = seconds_left % 3600;

        minutes = pad(parseInt(seconds_left / 60));
        seconds = pad(parseInt(seconds_left % 60));
        return {
            days: days,
            hours: hours,
            minutes: minutes,
            seconds: seconds
        }
    };
}
// кол-во нулей
function pad(n) {
    return (n < 10 ? '0' : '') + n;
}

function innerHelper(stock, timeLeft, count){
    let days = stock.querySelector(".timer .day .number");
    let hours = stock.querySelector(".timer .hour .number");
    let minutes = stock.querySelector(".timer .min .number");
    let seconds = stock.querySelector(".timer .sec .number");
    let inStock = stock.querySelector(" .timer .count .number");

    days ? days.innerText != timeLeft.days ? days.innerText = timeLeft.days : "" : ""
    hours ? hours.innerText != timeLeft.hours ? hours.innerText = timeLeft.hours : "" : ""
    minutes ? minutes.innerText != timeLeft.minutes ? minutes.innerText = timeLeft.minutes : "" : ""
    seconds ? seconds.innerText != timeLeft.seconds ? seconds.innerText = timeLeft.seconds : "" : ""
    inStock ? inStock.innerText != count ? inStock.innerText = count : "" : ""
}


// основная функция по вызову всего
for(let i = 0; i < stockTime.length; i++){
    let randomData = randomNumber();
    setInterval(function () {
        let countTime = getCountdown(randomData);
        let timeLeft = countTime();
        innerHelper(stockTime[i], timeLeft, count)
    }, 1000);
    
}


