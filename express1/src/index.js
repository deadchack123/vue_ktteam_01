import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import initializeDb from './db';
import middleware from './middleware';
import api from './api';
import config from './config.json';
import "isomorphic-fetch"
const fetch = require('node-fetch');


let app = express();
app.server = http.createServer(app);

// logger
app.use(morgan('dev'));

// 3rd party middleware
app.use(cors({
	exposedHeaders: config.corsHeaders
}));

app.use(bodyParser.json({
	limit: config.bodyLimit
}));

// connect to db
initializeDb(db => {

	// internal middleware
	app.use(middleware({ config, db }));

	// api router
	app.use('/api', api({ config, db }));

	app.use('/api/catalog/:index/:entityType/_search', (req, res, next) => {
		let query = req._parsedUrl.query || ""
		
		if (!req.params.index || !req.params.entityType) {
			res.status(500)
			res.send("Entity type and index name is required")
		}
		if (req.method !== "GET" && req.method !== "POST") {
			res.status(500)
			res.send("ПОшел нахуй с таким типом запроса")
		}

		let url = `${req.params.index}/${req.params.entityType}/_search`

		fetch(`${config.auth}@${config.elastic}/${url}?${query}`, {
			method: "POST",
			body: JSON.stringify(req.body) || {},
			headers: {
				'Content-Type': 'application/json',
			},
		}).then(response => {
			return response.json()
		}).then(json => {
			json.hits.hits = json.hits.hits.map(item => {
				return item._source
			})
			json.hits.aggregations = json.aggregations
			res.send(json.hits)
			next()
		}).catch((error) => res.send(error))
	});

	app.server.listen(process.env.PORT || config.port, () => {
		console.log(`Started on port ${app.server.address().port}`);
	});
});

export default app;
