import Vue from "vue";
import Router from "vue-router";
import ProductList from "@/views/ProductList";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    // { path: '/', redirect: '/1', },
    // { name:"start-page", path: "/1", component: ProductList, alias: "" },
    { name: "catalog-page", path: "/:page", component: ProductList }
  ]
});
