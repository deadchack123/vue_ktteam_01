const fetch = require('node-fetch');
import "isomorphic-fetch"
var bodybuilder = require('bodybuilder')


export default {
    state: {
        items: [],
        total: 0,
        size: 3,
        from: 0,
        currentPage: 1,
        filterValue: {},
        sortBy: [
        {
            value: "discount.price",
            description:"Цене скидки"
          },
          {
            value:"price",
            description:"Цене"
          },
          {
            value: "stock",
            description: "Колличеству"
          }
        ]  
      ,
        aggRange: {
            minPrice: 0,
            maxPrice: 99999,
            minCount: 0,
            maxCount: 999
        },
        url: 'http://localhost:8079/api/catalog',
    },
    mutations: {
        SET_ITEMS(state, json) {
            if(json.aggregations){
                state.aggRange.maxPrice = json.aggregations['agg_max_discount.price'].value
                state.aggRange.minPrice = json.aggregations['agg_min_discount.price'].value
                state.aggRange.maxCount = json.aggregations['agg_max_stock'].value
                state.aggRange.minCount = json.aggregations['agg_min_stock'].value
            }
            state.total = json.total
            state.items = json.hits
        },
        SET_CURRENT_PAGE(state, page) {
            state.currentPage = page
        },
        SET_QUERY(state, queryParams) {
            state.filterValue = queryParams.filter
            state.sort = queryParams.sort
        }
    },
    actions: {
        loadItems({ commit, state }, { page = 1, sort = '', filter = {} }) {
            let from = +page - 1
            let query = filter !== {} ? bodybuilder()
                .filter('range', 'discount.price', { gte: filter.minPrice, lte: filter.maxPrice })
                .filter('range', 'stock', { gte: filter.minCount, lte: filter.maxCount })
                .aggregation('max', 'discount.price')
                .aggregation('min', 'discount.price')
                .aggregation('max', 'stock')
                .aggregation('min', 'stock')
                .build() : bodybuilder()
                    .aggregation('max', 'discount.price')
                    .aggregation('min', 'discount.price')
                    .aggregation('max', 'stock')
                    .aggregation('min', 'stock')
                    .build()
            fetch(`${state.url}/products/product/_search/?size=${state.size}&from=${from * state.size}&sort=${sort}`, {
                method: 'POST',
                body: JSON.stringify(query),
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then(response => response.json())
                .then(json => {
                    commit('SET_ITEMS', json)
                    commit('SET_CURRENT_PAGE', page)
                    commit('SET_QUERY', { sort, filter })
                    commit('SET_LOADER')
                })
                .catch((err) => {
                    console.error(err)
                })
        }
    }
}