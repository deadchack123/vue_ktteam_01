import Vue from 'vue'
import Vuex from 'vuex'
import products from './Products.js'
import "isomorphic-fetch"
const fetch = require('node-fetch');

Vue.use(Vuex)

export default new Vuex.Store({
  modules:{
    products
  },
  state: {
    payItems: [
      {
        logo: './images/yndex.jpg',
        name: 'Яндекс Деньги',
        discription: '0% коммисия'
      },
      {
        logo: './images/eleksnet.jpg',
        name: 'Элекснет',
        discription: '0% коммисия'
      },
      {
        logo: './images/webmoney.jpg',
        name: 'Webmoney',
        discription: '0.8% коммисия'
      },
      {
        logo: './images/qiwi.jpg',
        name: 'Qiwi',
        discription: 'до 10% коммисия'
      },
      {
        logo: './images/visa-mastercard.jpg',
        name: 'Карты Visa, Mastercard (процессинг Uniteller)',
        discription: '0% коммисия'
      },
      {
        logo: './images/novoplat.jpg',
        name: 'Новоплат',
        discription: 'до 4% коммисия'
      },
    ],
    isModalVisible: false,
    filterVisible: false,
    loader: true
  },
  mutations: {
    SHOW_MODAL(state) {
      state.isModalVisible = true
    },
    CLOSE_MODAL(state) {
      state.isModalVisible = false
    },
    VISIBLE_FILTER_MODAL(state) {
      state.filterVisible = !state.filterVisible
    },
    SET_LOADER(state){
      state.loader = false
    }
  },
  actions: {
    showModal({ commit }) {
      commit('SHOW_MODAL')
    },
    closeModal({ commit }) {
      commit('CLOSE_MODAL')
    },
    visibleFilterModal({ commit }) {
      commit('VISIBLE_FILTER_MODAL')
    },
  },

})
